/**
 * @author: Anil Kumar Ch
 * @Date: 24-06-2021
 */
const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '/.env') });
var createError = require('http-errors');
var express = require('express');
var cookieParser = require('cookie-parser');
var morgan = require('morgan');
const cors = require('cors');
const winston = require('winston');

var routers = require('./routes/index');

var app = express();

// resuming on uncaught exception means in application any where any exception came here it will be catch
process.on('uncaughtException', function (err) {
    logger.error('uncaughtException in app.js is------', err);
});


app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Cross-Origin Resource Sharing
app.use(cors({ origin: '*' }));

const logger = winston.createLogger({
    transports: [
        new winston.transports.Console({
            json: true,
            colorize: true,
            level: 'info'
        })
    ],
    exitOnError: false
});

global.logger = logger;

app.use('/api', routers);
app.get('/', function response(req, res) {
    res.status(200).send('Hello Backend');
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

app.get('*', function response(req, res) {
    res.status(404).send();
});
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

module.exports = app;
