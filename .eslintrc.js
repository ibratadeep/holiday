module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es2021: true,
    node: true
  },
  extends: [
    'standard'
  ],
  parserOptions: {
    ecmaVersion: 12
  },
  rules: {
    'global-require': [
      'error'
    ],
    'handle-callback-err': [
      'error'
    ],
    semi: [
      'error',
      'always'
    ]
  },
  globals: {
    logger: 'readonly'
  }

};
